# XS-Decor #

XS stands for extra small. It have this name, because its a extra small version of xdecor.

A decoration mod meant to be simple and well-featured.

It adds a bunch of cute cubes, various mechanisms cooking, etc.

This mod is a lightweight alternative to Home Decor.

If you want more features you can install the following mods:

* https://github.com/minetest-mods/realchess
* https://github.com/minetest-mods/enchanting
* https://github.com/minetest-mods/workbench
* https://github.com/minetest-mods/craftguide

Or install the overblown original xdecor mod.


### Credits ###

Special thanks to Gambit for the textures from the PixelBOX pack for Minetest.

Thanks to all contributors that keep this mod alive.

![Preview](http://i.imgur.com/AVoyCQy.png)
